1) Explicit run in console
java -cp "clipackj-1.0-SNAPSHOT.jar"  org.home.pack.App

2) Jar as executable
java -jar clipackj-1.0-SNAPSHOT.jar

3) Make a release
git subtree push --prefix dist origin gh-pages