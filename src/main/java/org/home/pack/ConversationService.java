package org.home.pack;

import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;
import org.jgroups.util.Util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

public class ConversationService extends ReceiverAdapter {

    JChannel channel;
    final List<String> state=new LinkedList<String>();

    public void viewAccepted(View new_view) {
        System.out.println("** view: " + new_view);
    }

    public void receive(Message msg) {
        String line=msg.getSrc() + ": " + msg.getObject();
        System.out.println(line);
        synchronized(state) {
            state.add(line);
        }
    }

    public void getState(OutputStream output) throws Exception {
        synchronized(state) {
            Util.objectToStream(state, new DataOutputStream(output));
        }
    }

    @SuppressWarnings("unchecked")
    public void setState(InputStream input) throws Exception {
        List<String> list=Util.objectFromStream(new DataInputStream(input));
        synchronized(state) {
            state.clear();
            state.addAll(list);
        }
        System.out.println("received state (" + list.size() + " messages in chat history):");

        for (String s : list) {
            System.out.println(s);
        }
    }

    public void start() throws Exception {

        String overrideChannelSettings =System.getProperty("transaport", "");

        if(overrideChannelSettings == "")
            channel = new JChannel();
        else
        {
            System.out.println("Transport :" + overrideChannelSettings);
            channel = new JChannel(overrideChannelSettings);
        }

        channel.setReceiver(this);
        channel.connect("ChatCluster");
        channel.getState(null, 10000);
    }

    public void stop() {
        channel.close();
    }

    public void send(String line) throws Exception{
        Message msg=new Message(null, line);
        channel.send(msg);
    }
}
