package org.home.pack;

import java.io.*;

public class App
{
    public static void main(String[] args) throws Exception {

        String user_name=System.getProperty("user.name", "n/a");

        ConversationService convesator = new ConversationService();
        convesator.start();

        BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            try {
                System.out.print("> "); System.out.flush();
                String line=in.readLine().toLowerCase();
                if(line.startsWith("quit") || line.startsWith("exit")) {
                    break;
                }
                line="[" + user_name + "] " + line;

                convesator.send(line);

            }
            catch(Exception e) {
            }
        }

        convesator.stop();
    }
}
